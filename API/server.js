require('dotenv').config()
var cors = require('cors')
const fetch = require('node-fetch')
global.fetch = fetch
const express = require('express')
const app = express()
app.use(cors())
const bodyParser = require('body-parser')
// create application/json parser
const { Sequelize } = require('sequelize')
const routes = require('./routes/index')

require('./routes')(app)

const sequelize = new Sequelize(
    `${process.env.aws_rds_db}`,
    `${process.env.aws_rds_username}`,
    `${process.env.aws_rds_pass}`,
    {
        host: `${process.env.aws_rds_host}`,
        dialect: 'postgres',
    }
)

sequelize
    .authenticate()
    .then(function (err) {
        console.log('Connection has been established successfully.')
    })
    .catch(function (err) {
        console.log('Unable to connect to the database:', err)
    })

app.listen(8000)

console.log('listening to 8000')
