const GetCityData = require('../controllers/getCityData')
const Unsplash = require('../controllers/Unsplash')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const Behance = require('../controllers/Behance')

module.exports = function (app) {
    app.post('/unsplash', jsonParser, async function (req, res) {
        const unsplash = await new Unsplash(req.body.data)
        const bg_images = await unsplash.init()
        res.status(200).send(bg_images)
    })

    app.post('/getlocation', jsonParser, async function (req, res) {
        const getcitydata = await new GetCityData(
            req.body.latitude,
            req.body.longitude
        )
        const retData = await getcitydata.init()
        await res.status(200).send(retData.data.results[0].locations)
        // await res.status(404).send({ error: 'error' })
    })

    app.get('/behance', jsonParser, async function (req, res) {
        const behanceObj = await new Behance()
        const profile = await behanceObj.init()
        await res.status(200).send(profile)
    })

    app.get('/behanceProjects', jsonParser, async function (req, res) {
        const behanceObj = await new Behance()
        const profile = await behanceObj.getProject()
        await res.status(200).send(profile)
    })

    app.get('/getProject', jsonParser, async function (req, res) {
        const behanceObj = await new Behance(req.query['0'])
        const project = await behanceObj.getSingleProject()
        console.log(project)
        await res.status(200).send(project)
    })
}
