const Axios  = require('axios');

class GetCityData {

    constructor(lat, long) {
        this.lat = lat;
        this.long = long;
        this.key = process.env.mapquest_sk;
        this.data = null
        this.endPoint = `http://open.mapquestapi.com/geocoding/v1/reverse?key=${this.key}&location=${this.lat},${this.long}&includeRoadMetadata=true&includeNearestIntersection=true`
    }

    init(){

        return Axios({
            method: 'get',
            url: this.endPoint
          }).then((data)=>{
            return data;
          }).catch((e)=>{
            console.log("error", e)
          })
    }

}


module.exports = GetCityData;