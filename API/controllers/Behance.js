const axios = require('axios')

class Behance {
    constructor(projectId) {
        this.projectId = projectId
        this.getUserEndpoint = `http://behance.net/v2/users/wramirez09?api_key=${process.env.behance_apiKey}`
        this.getProjectsEndPoint = `http://www.behance.net/v2/users/wramirez09/projects?api_key=${process.env.behance_apiKey}`
        this.singleProjectEndPoint = `http://www.behance.net/v2/projects/${this.projectId}?api_key=${process.env.behance_apiKey}`
    }

    init() {
        return axios({
            method: 'get',
            url: this.getUserEndpoint,
        })
            .then((data) => {
                return data.data
            })
            .catch((e) => {
                console.log('error', e)
            })
    }

    getProject() {
        return axios({
            method: 'get',
            url: this.getProjectsEndPoint,
        })
            .then((data) => {
                return data.data
            })
            .catch((e) => {
                console.log('error', e)
            })
    }

    getSingleProject() {
        return axios({
            method: 'get',
            url: this.singleProjectEndPoint,
        })
            .then((data) => {
                return data.data
            })
            .catch((e) => {
                console.log('error', e)
            })
    }
}

module.exports = Behance
