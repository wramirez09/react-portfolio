const axios = require('axios').default

class Unsplash {
    constructor(query) {
        this.unsplashEndPoint = `https://api.unsplash.com/search/photos?client_id=${
            process.env.usplash_ak
        }&query=${encodeURI(query.place)}?orientation=${
            query.orientation
        }?sig=1`
        // console.log("query url", this.unsplashEndPoint)
    }

    async init() {
        try {
            const response = await axios.get(this.unsplashEndPoint)
            console.log('resp', this.unsplashEndPoint)
            return response.data.results
        } catch (error) {
            console.log(error)
        }
    }
}

module.exports = Unsplash
