import { configureStore } from '@reduxjs/toolkit'
import toggleLoaderReducer from './Reducers/loaderSlice'
import unsplashSlice from './Reducers/unsplashSlice'
import projectsSlice from './Reducers/projectsSlice'
import bioSlice from './Reducers/bioSlice'

export default configureStore({
    reducer: {
        toggleLoaderReducer: toggleLoaderReducer,
        unsplashSlice: unsplashSlice,
        projectsSlice: projectsSlice,
        bioSlice: bioSlice,
    },
})
