import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    bio: { name: 'will ramirez' },
}

const bioSlice = createSlice({
    name: 'bioSlice',
    initialState,
    reducers: {
        updateBioStore(state, action) {
            state.bio = action.payload
        },
    },
})

export default bioSlice.reducer
export const { updateBioStore } = bioSlice.actions
