import { createSlice } from '@reduxjs/toolkit';


const initialState = {
    bgImageData:[{  
        bio: "Graphic designer based in Liverpool.",
        first_name: "Neil",
        imageUrl: "https://images.unsplash.com/photo-1567976603810-d286866b26e8?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjE0MDY1MX0",
        last_name: "Martin",
        location: undefined,
        portfolio_url: "https://www.flickr.com/photos/theworkof/",
        profile_image: "https://images.unsplash.com/profile-fb-1521846539-8b66184b5bb9.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=32&w=32",

        urls:{
            raw:"https://images.unsplash.com/photo-1552004114-337ee51baa86?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjE0MDY1MX0",
            full:"https://images.unsplash.com/photo-1552004114-337ee51baa86?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjE0MDY1MX0",
            regular:"https://images.unsplash.com/photo-1552004114-337ee51baa86?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjE0MDY1MX0",
            small:"https://images.unsplash.com/photo-1552004114-337ee51baa86?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjE0MDY1MX0",
            thumb:"https://images.unsplash.com/photo-1552004114-337ee51baa86?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjE0MDY1MX0",
            self:"https://api.unsplash.com/photos/PdmlvIuJujc",
            html:"https://unsplash.com/photos/PdmlvIuJujc",
            download:"https://unsplash.com/photos/PdmlvIuJujc/download",
            download_location:"https://api.unsplash.com/photos/PdmlvIuJujc/download",
        }
    }],
};

const unsplashSlice = createSlice({
    name: "unsplashSlice",
    initialState,
    reducers:{
        updateBgData(state, action) {

            state.bgImageData = action.payload
        }
    },
   
})


export default unsplashSlice.reducer
export const { updateBgData } = unsplashSlice.actions