import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    projects: { project: 'lorem' },
}

const projectsSlice = createSlice({
    name: 'projectsSlice',
    initialState,
    reducers: {
        updateProjectsStore(state, action) {
            state.projects = action.payload
        },
        getProjectsSlice(state) {
            return state.projects
        },
    },
})

export default projectsSlice.reducer
export const { updateProjectsStore } = projectsSlice.actions
