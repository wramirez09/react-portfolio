import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    toggleLoader: true
}

const toggleLoaderSlice = createSlice({
  name: 'toggleLoader',
  initialState,
  reducers: {
    toggleLoader: (state, action)=>{
        state.toggleLoader = state.toggleLoader === true ? false : true;
      }
  },
  toggleLoader: (state, action)=>{
      state.toggleLoader = state.toggleLoader === true ? false : true;
  }
})
export const { toggleLoader} = toggleLoaderSlice.actions
export default toggleLoaderSlice.reducer