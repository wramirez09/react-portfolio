import React from 'react'
import { Jumbotron, Container, Row, Form, Button } from 'react-bootstrap'
export default function CreateProject() {
    return (
        <div>
            <Jumbotron style={{ margin: '100px 0 0' }}>
                <Container>
                    <Row style={{ margin: '0 0 50px' }}>
                        <h1>Login</h1>
                    </Row>
                    <Row>
                        <Form>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="Enter email"
                                />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone
                                    else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                />
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                    </Row>
                </Container>
            </Jumbotron>
        </div>
    )
}
