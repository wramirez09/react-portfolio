import React, { useState, useEffect, Component, Fragment } from 'react'
import Axios from 'axios'
import Container from 'react-bootstrap/Container'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import Work from '../components/Work'

export default class SingleProject extends Component {
    constructor(props) {
        super(props)
        this.id = this.props.location.state.id
        this.props = props
        this.endpoint = `http://localhost:8000/getProject/`
        this.state = {}
    }

    getProjects() {
        Axios.get(this.endpoint, {
            params: this.id,
        })
            .then((projectData) => {
                this.setState(projectData.data.project)

                console.log('this state', this.state)
            })
            .catch((e) => {
                return console.log('error', e)
            })
    }
    //
    componentDidMount() {
        this.getProjects()
        console.log('props', this.props)
    }

    componentWillUnmount() {}

    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):
        if (this.props.location.state.id !== prevProps.location.state.id) {
            this.id = this.props.location.state.id
            this.setState({ id: this.props.location.state.id })
            this.getProjects()
        }
    }
    render() {
        if (this.state.name) {
            return (
                <Fragment>
                    <Jumbotron style={{ margin: '100px 0 100px' }}>
                        <Container>
                            <Row className="cardRow">
                                <Col xs={6}>
                                    <Image
                                        src={this.state.covers['original']}
                                    ></Image>
                                </Col>
                                <Col xs={6}>
                                    <h1>{this.state.name}</h1>
                                    <p>{this.state.description}</p>
                                </Col>
                            </Row>
                            <hr />
                        </Container>
                    </Jumbotron>
                    <Container>
                        <Work></Work>
                    </Container>
                </Fragment>
            )
        } else {
            return <h1>waiting on data</h1>
        }
    }
}
