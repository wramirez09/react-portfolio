import React, { Fragment } from 'react'
import SplashIntro from '../components/SplashIntro'
import AboutSection from '../components/AboutSection'
import ContactSection from '../components/ContactSection'
import { useSelector } from 'react-redux'
import Work from '../components/Work'
import Loader from '../components/Loader'
import Navbar from 'react-bootstrap/Navbar'

const Home = () => {
    const loaderState = useSelector(
        (state) => state.toggleLoaderReducer.toggleLoader
    )
    const bgImageSlice = useSelector((state) => {
        return state.unsplashSlice.bgImageData
    })
    const headline = `“I want to make beautiful things,<br> even if nobody cares.”<br> <small>― Saul Bass.</small>`
    let createMarkup = () => {
        return { __html: headline }
    }

    return (
        <Fragment>
            <SplashIntro></SplashIntro>
            <Work></Work>
        </Fragment>
    )
}

export default Home
