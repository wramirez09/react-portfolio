import React from 'react'

const Context = React.createContext();

// export const ContextProvider = Context.Provider
// export const ContextConsumer = Context.Consumer

export default Context;





class ContextProvider extends React.Component {
    constructor(props){
        super();
        this.state = {
            isloaderVisible:true,
        }
    }
    
    setContext = (isloaderVisible) => {
        this.setState((prevState) => ({ isloaderVisible }))
      }
    render() {

        const { children } = this.props
        const context = this.state
        const setContext = this
        
        return(
            <Context.Provider value={{context, setContext}}>
                {children}
            </Context.Provider>
        )
    }
}

export { ContextProvider }