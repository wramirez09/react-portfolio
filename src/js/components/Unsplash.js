import React, { useState, useEffect, Fragment } from 'react'
import { useDispatch } from 'react-redux'
import Axios from 'axios'
import Photographer from '../components/Photographer'
import RenderData from '../components/RenderData'
import FrostImage from './FrostImage'
import caret from '../../images/down-arrow.svg'
import gsap from 'gsap'
import { ScrollToPlugin } from 'gsap/all'
import CalAspectRation from '../components/CalAspectRatio'
import Jumbotron from 'react-bootstrap/Jumbotron'
import { toggleLoader } from '../Redux/Reducers/loaderSlice'
import { updateBgData } from '../Redux/Reducers/unsplashSlice'
import { useSelector } from 'react-redux'
import rn from '../Helpers/randNumbByArray'
gsap.registerPlugin(ScrollToPlugin)

const Unsplash = (props) => {
    const dispatch = useDispatch()
    const endpoint = 'http://localhost:8000/unsplash'
    const [bgImage, setBgImage] = useState({})
    const [data, setdata] = useState(null)
    const bgImageSlice = useSelector((state) => {
        return state.unsplashSlice.bgImageData
    })
    const toggleLoaderSlice = useSelector((state) => {
        return state.toggleLoaderReducer.toggleLoader
    })

    let init = () => {
        console.log('init unsplsh comp mounted')
        if (bgImageSlice.length > 1) {
            console.log('using stored image')
            const randn = rn(bgImageSlice)
            const images = bgImageSlice
            const bgImagedata = {
                imageUrl:
                    CalAspectRation() === 'landscape'
                        ? images[randn].urls.full
                        : images[randn].urls.small,
                location: images[randn].location,
                first_name: images[randn].user.first_name,
                last_name: images[randn].user.last_name,
                profile_image: images[randn].user.profile_image.small,
                bio: images[randn].user.bio,
                portfolio_url: images[randn].user.portfolio_url,
            }
            // set bg image state
            setBgImage(bgImagedata)

            if (toggleLoaderSlice === true) {
                dispatch(toggleLoader())
            } else {
                // dispatch(toggleLoader())
            }
        } else if (navigator.geolocation) {
            console.log('using navigator')
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    console.log('success! uisng client position ', position)
                    success(position)
                },
                (error) => {
                    errorFunc(error)
                }
            )
        } else {
            console.log('no location support')
            if (bgImageSlice.length > 1) {
                console.log(
                    'navigator not supported using fall back random image',
                    bgImageSlice
                )
                const randn = rn(bgImageSlice)
                const images = bgImageSlice
                const bgImagedata = {
                    imageUrl:
                        CalAspectRation() === 'landscape'
                            ? images[randn].urls.full
                            : images[randn].urls.small,
                    location: images[randn].location,
                    first_name: images[randn].user.first_name,
                    last_name: images[randn].user.last_name,
                    profile_image: images[randn].user.profile_image.small,
                    bio: images[randn].user.bio,
                    portfolio_url: images[randn].user.portfolio_url,
                }
                // set bg image state
                setBgImage(bgImagedata)
                if (toggleLoaderSlice === true) {
                    dispatch(toggleLoader())
                }
            }
        }
    }

    let success = async (pos) => {
        console.log('success func called', pos)
        const endpoint = `http://localhost:8000/getlocation`

        await Axios({
            method: 'post',
            url: endpoint,
            data: {
                latitude: pos.coords.latitude,
                longitude: pos.coords.longitude,
            },
        })
            .then((data) => {
                console.log('data returned', data)
                setdata(data)
                getBg(data)
                dispatch(toggleLoader())
            })
            .catch((e) => {
                errorFunc(e)
            })
    }

    let errorFunc = (e) => {
        if (e) console.log('error ', e)

        console.log('stored bg image being used', bgImageSlice)
        if (bgImageSlice.length > 0) {
            // setBgImage(bgImageSlice)
            console.log('get error fall back image in place ', bgImageSlice)
            const randn = rn(bgImageSlice)
            const images = bgImageSlice
            const bgImagedata = {
                imageUrl:
                    CalAspectRation() === 'landscape'
                        ? images[randn].urls.full
                        : images[randn].urls.small,
                location: images[randn].location,
                first_name: images[randn].first_name,
                last_name: images[randn].last_name,
                profile_image: images[randn].profile_image.small,
                bio: images[randn].bio,
                portfolio_url: images[randn].portfolio_url,
            }
            // set bg image state
            setBgImage(bgImagedata)
            dispatch(toggleLoader())
        }
    }

    let getBg = async (location) => {
        console.log('getting background image based on location', location)
        const imageData = {
            place: location.data[0].adminArea5,
            orientation: CalAspectRation(),
        }

        await Axios.post(endpoint, {
            data: imageData,
        }).then((images) => {
            const randn = rn(images.data)

            if (images.data.length !== 0) {
                dispatch(updateBgData(images.data))

                const bgImagedata = {
                    imageUrl:
                        CalAspectRation() === 'landscape'
                            ? images.data[randn].urls.full
                            : images.data[randn].urls.small,
                    location: images.data[randn].location,
                    first_name: images.data[randn].user.first_name,
                    last_name: images.data[randn].user.last_name,
                    profile_image: images.data[randn].user.profile_image.small,
                    bio: images.data[randn].user.bio,
                    portfolio_url: images.data[randn].user.portfolio_url,
                }
                // set bg image state
                setBgImage(bgImagedata)
                console.log('bg image data set', bgImagedata)
            }
        })
    }

    // render views

    let showCaret = () => {
        const caret = document.querySelector('.unsplash__caret')

        if (caret) {
            gsap.to([caret], 1, { opacity: 1, ease: 'bounce.out' })
        }
    }
    let scrollToAboutSection = () => {
        console.log('caret click')
        if (window) {
            gsap.to([window], 1, {
                scrollTo: '.aboutSection',
                onComplete: () => {
                    gsap.to(['.aboutSection__copy'], 1, { opacity: 1 })
                },
            })
        }
    }

    let unsplash = (data) => {
        const unsplash = document.querySelector('.unsplash')
        const client = window || null
        const clientHeight = client.innerHeight

        if (unsplash) {
            gsap.to([unsplash], 0.5, { opacity: 1 })
            showCaret()
        }

        const heroHeight = clientHeight + 'px'

        if (bgImage) {
            return (
                <Fragment>
                    <Jumbotron
                        fluid={true}
                        style={{
                            background: `url(${bgImage.imageUrl}) no-repeat`,
                            width: '100%',
                            height: `${heroHeight}`,
                        }}
                    >
                        <FrostImage
                            headline={props.headline.__html}
                            backgroundImage={bgImage.imageUrl}
                            buttonText="About"
                        ></FrostImage>
                        <img
                            src={caret}
                            width="50px"
                            height="40px"
                            alt="down-arrow"
                            className="unsplash__caret"
                            onClick={scrollToAboutSection}
                        />
                        <RenderData data={data}></RenderData>
                    </Jumbotron>

                    <Photographer {...bgImage}></Photographer>
                </Fragment>
            )
        }
    }

    useEffect(() => {
        init()
    }, [])

    return ((data) => {
        if (true) {
            return unsplash(data)
        } else {
            return console.log('no data yet')
            // return showLoader()
        }
    })(data)
}

export default Unsplash
