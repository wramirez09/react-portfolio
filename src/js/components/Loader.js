import React, { useState, useEffect, Fragment } from 'react'
import { useSelector } from 'react-redux'

const Loader = () => {
    const [state, updateState] = useState({ active: true })
    const loaderSlice = useSelector((state) => {
        return state.toggleLoaderReducer.toggleLoader
    })

    let init = () => {
        updateState(loaderSlice)
    }

    useEffect(() => {
        init()
    }, [])

    return (
        <Fragment>
            {(() => {
                if (loaderSlice) {
                    return (
                        <div className="loader">
                            <div className="multi-spinner-container">
                                <div className="multi-spinner">
                                    <div className="multi-spinner">
                                        <div className="multi-spinner">
                                            <div className="multi-spinner">
                                                <div className="multi-spinner">
                                                    <div className="multi-spinner"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                } else {
                    return console.log('loader off if false', loaderSlice)
                }
            })()}
        </Fragment>
    )
}

export default Loader
