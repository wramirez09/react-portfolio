import React from 'react';
import gsap from 'gsap';

const Remderdata = (props)=>{

    const styles = {
        background:'#333',
        color:'white',
        display:'inline-block',
        padding: '5px 10px',
        position: 'absolute',
        right:0,
        top:'56px'
    }

    

    let _this = document.querySelector('.location');

    if(_this){
        gsap.set([_this], {right:"-50vw"});
        gsap.to([_this], 2, {right:0, opacity: 1});

    }
    
    
    return (
        (()=>{
            if(props.data){
                return(
                    <aside className='location' style={{...styles}}>
                    <span>{props.data.data[0].adminArea5}</span>
                    </aside>
                )
            }
            else{
                return <p>loading</p>
            }
        })(props)
    )
}

export default Remderdata;