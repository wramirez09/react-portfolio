import React from 'react'
import bgImage from '../../images/unsplash_bg_1.jpg'
import FrostImage from './FrostImage'
import Jumbotron from 'react-bootstrap/Jumbotron'
export default function SplashIntro(props) {
    const client = window || null
    const clientHeight = client.innerHeight
    return (
        <Jumbotron
            fluid={true}
            style={{
                background: `url(${bgImage}) no-repeat`,
                width: '100%',
                height: `${clientHeight}px`,
            }}
        >
            <FrostImage
                headline={
                    'I want to make beautiful things,<br> even if nobody cares.”<br> <small>― Saul Bass.</small>'
                }
                backgroundImage={bgImage}
                buttonText="About"
            ></FrostImage>
        </Jumbotron>
    )
}
