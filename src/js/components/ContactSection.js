import React from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Jumbotron from 'react-bootstrap/Jumbotron'
export default function ContactSection() {
    return (
        <section className="contactSection">
            <Container>
                <Row>
                    <Col>
                        <Jumbotron>
                            <h1>contact</h1>
                        </Jumbotron>
                        <form
                            className={'classes.root'}
                            noValidate
                            autoComplete="off"
                        >
                            <TextField
                                id="standard-basic"
                                label="Name"
                                fullWidth
                                styles={{ margin: '10px' }}
                            />
                            <TextField
                                id="filled-basic"
                                label="Email"
                                variant="filled"
                                fullWidth
                                styles={{ margin: '10px' }}
                            />
                            <TextField
                                id="outlined-basic"
                                label="message"
                                variant="outlined"
                                fullWidth
                                styles={{ margin: '10px' }}
                            />
                            <Button color="primary">Submit</Button>
                        </form>
                    </Col>
                </Row>
            </Container>
        </section>
    )
}
