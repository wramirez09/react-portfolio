import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import { updateProjectsStore } from '../Redux/Reducers/projectsSlice'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import gsap, { TimelineMax } from 'gsap'
export default function Work() {
    const [projects, updateProjectsState] = useState(null)

    const dispatch = useDispatch()
    let projectsEndpoint = 'http://localhost:8000/behanceProjects/'
    const projectSlice = useSelector((state) => {
        return state.projectsSlice
    })
    console.log('projectSlice', projectSlice.projects)
    let getProjects = async () => {
        if (projectSlice.projects.length >= 1) {
            console.log(projectSlice.projects)
            updateProjectsState(projectSlice.projects)
        } else {
            return await Axios.get(projectsEndpoint, {})
                .then((data) => {
                    console.log('behance project data returned', data)
                    updateProjectsState(data.data.projects)
                    dispatch(updateProjectsStore(data.data.projects))
                    return data
                })
                .then(() => {})
                .catch((e) => {
                    return console.log('error', e)
                })
        }
    }

    let onMouseOverAnimation = (target) => {
        let tl = new TimelineMax()
        let titleBar = target.firstChild
        const tl2 = new TimelineMax()

        tl.set([target], { backgroundSize: '110%' })
        tl.set([titleBar], { height: '56px' })

        tl.fromTo(
            [target],
            { backgroundSize: '110%' },
            {
                backgroundSize: '150%',
                duration: 3,
            }
        )

        tl2.to([titleBar], 1, { height: '15%' })
        tl2.to([titleBar.firstChild], 1, { scale: 0.75 }, '-=1')
    }

    let onMouseOutAnimation = (target) => {
        let tl = new TimelineMax()

        tl.fromTo(
            [target],
            { backgroundSize: '150%' },
            { backgroundSize: 'cover', duration: 3 }
        )

        let titleBar = target.firstChild
        const tl2 = new TimelineMax()

        tl2.fromTo(
            [titleBar],
            { height: '16%' },
            { height: '56px', duration: 1 }
        )
        tl2.to([titleBar.firstChild], 1, { scale: 1 }, '-=1')
    }

    let animateBg = (target) => {
        if (target.classList.contains('projects-grid__project')) {
            onMouseOverAnimation(target)
        } else if (target.classList.contains('projects-grid__title-bar')) {
            onMouseOverAnimation(target.parentNode)
        }
    }

    let animateBgBack = (target) => {
        let tl = new TimelineMax()
        if (target.classList.contains('projects-grid__project')) {
            onMouseOutAnimation(target)
        } else if (target.classList.contains('projects-grid__title-bar')) {
            onMouseOutAnimation(target.parentNode)
        }
    }

    useEffect(() => {
        getProjects()

        return () => {}
    }, [])

    return (
        <section>
            {(() => {
                if (projects) {
                    return (
                        <section className={`projects-grid`}>
                            {projects.map((project) => {
                                return (
                                    <Link
                                        onMouseEnter={(e) => {
                                            return animateBg(e.target)
                                        }}
                                        onMouseLeave={(e) => {
                                            return animateBgBack(e.target)
                                        }}
                                        to={{
                                            pathname: `/project/${project.slug}`,
                                            state: { id: project.id },
                                        }}
                                        key={project.id}
                                        className="projects-grid__project"
                                        style={{
                                            background: `url(${project.covers['original']}) no-repeat`,
                                            backgroundSize: 'cover',
                                            backgroundPosition: 'center',
                                            width: '33.1%',
                                            height: '300px',
                                        }}
                                    >
                                        <div
                                            className="projects-grid__title-bar"
                                            onMouseEnter={(e) => {
                                                return animateBg(e.target)
                                            }}
                                            onMouseLeave={(e) => {
                                                return animateBgBack(e.target)
                                            }}
                                        >
                                            <span className="projects-grid__title-bar-title">
                                                {project.name}
                                            </span>
                                        </div>
                                    </Link>
                                )
                            })}
                        </section>
                    )
                }
            })()}
        </section>
    )
}
