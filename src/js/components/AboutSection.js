import React, { useState, useEffect, Fragment } from 'react'
import Axios from 'axios'
import { useSelector, useDispatch } from 'react-redux'
import { updateBioStore } from '../Redux/Reducers/bioSlice'
import FrostImage from './FrostImage'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import rn from '../Helpers/randNumbByArray'
const AboutSection = () => {
    const [bgImageData, setBgImageData] = useState({})
    const [bioDataHolder, setBioData] = useState({})
    const dispatch = useDispatch()
    const bioObj = bioDataHolder
    const bioendpoint = 'http://localhost:8000/behance'
    const client = window || null
    const clientHeight = client.innerHeight
    const bgImageSlice = useSelector((state) => {
        return state.unsplashSlice.bgImageData
    })

    let init = () => {
        const randNumbByArray = rn(bgImageSlice)
        const bgImageDataFromStore = bgImageSlice[randNumbByArray]
        setBgImageData(bgImageDataFromStore)
    }

    let getBioData = () => {
        Axios.get(bioendpoint, {})
            .then((bioData) => {
                console.log('behance bio data returned', bioData.data)
                setBioData(bioData.data.user)
                dispatch(updateBioStore(bioData.data.user))
            })
            .catch((e) => {
                return console.log('error', e)
            })
    }

    let renderBio = () => {
        if (bioDataHolder.images && bgImageData.urls !== undefined) {
            return (
                <section
                    className="aboutSection"
                    style={{
                        width: '100%',
                        height: clientHeight,
                        background: `url(${bgImageData.urls.full}) no-repeat`,
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                    }}
                >
                    {console.log('bgImageDataFromStore', bgImageData)}
                    <FrostImage
                        backgroundImage={bgImageData.urls.full}
                    ></FrostImage>
                    <Container>
                        <Row>
                            <Col>
                                <img
                                    src={bioDataHolder.images['276']}
                                    className="aboutSection__bio-pic"
                                    alt=""
                                />
                            </Col>
                            <Col className="">
                                <h1>
                                    {bioObj.first_name} {bioObj.last_name}
                                </h1>
                                <p>{bioObj.location}</p>
                                <p>{bioObj.sections.About}</p>
                            </Col>
                        </Row>
                    </Container>
                </section>
            )
        }
    }

    useEffect(() => {
        init()
        getBioData()
    }, [])

    return (
        <Fragment>
            {(() => {
                if (bgImageData) {
                    return renderBio()
                }
            })()}
        </Fragment>
    )
}

export default AboutSection
