
import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import CalAspectRation from "./CalAspectRatio";


const UnsplashQuery = (query)=>{

    const [bgQueryData, setQueryData] = useState({});
    const endpoint = `https://api.unsplash.com/search/photos?client_id=L13hpyybQyQ8Vqqm6vN-NhY2KFYdgINzengBYFMk34c&query=${query.place}?orientation=${query.orientation}`;;


        useEffect(()=>{
            
            Axios.post(endpoint, (data)=>{
                
            }).then((images)=>{
                console.log("images ",images)
                function getRandomArbitrary(min, max) {
                    return Math.random() * (max - min) + min;
                }
    
                const rn = Math.floor(getRandomArbitrary(1, images.length));
                
                const queryData  = {
                    imageUrl: CalAspectRation() === "landscape" ? images.data[rn].urls.full : images.data[rn].urls.small,
                    location: images.data[rn].location,
                    first_name: images.data[rn].user.first_name,
                    last_name: images.data[rn].user.last_name,
                    profile_image: images.data[rn].user.profile_image.small,
                    bio:images.data[rn].user.bio,
                    portfolio_url:images.data[rn].user.portfolio_url,
                }
    
                setQueryData(queryData)
                
            }).catch((e)=>{
                return console.log("error",e)
            })            
        }, [query, endpoint])


        return bgQueryData;
    }


export default UnsplashQuery;