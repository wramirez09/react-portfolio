
import { useState, useEffect } from 'react';

import Axios from 'axios';

const GeoLocation = ()=>{

    const [data, setdata] = useState(null);

    let init = ()=> {

        if(!navigator.geolocation) {
           
           console.log('no location support')
        }
        else{
            navigator.geolocation.getCurrentPosition(success, error);
        }
    }
    
    let success = async(pos)=>{
        
        const endpoint = `http://localhost:8000/getlocation`;
        
         await Axios({
            method: 'post',
            url: endpoint,
            data: {
              latitude: pos.coords.latitude,
              longitude: pos.coords.longitude,
            }
          }).then((data)=>{
            
            setdata(data)
            return data.data[0]
          })
    }

    let error = (e)=> {
        console.log('error ', e)
    }

    useEffect(()=>{
        init();
    },[]);

    return data;

}
 
export default GeoLocation;